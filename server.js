var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('Todo list RESTful API server started on: ' + port);

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'lectura.json'));
});

//app.get('/clientes/:idcliente', function(req, res){
//  res.send('Aquí tiene el cliente número ' + req.params.idcliente);
//});

app.get('/clientes/:idcliente', function(req, res){
  res.sendFile(path.join(__dirname, 'cliente'+req.params.idcliente+'.json'));
});

app.post('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'alta.json'));
});

app.put('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'update.json'));
});

app.delete('/clientes/:idcliente', function(req, res){
  res.send('Se ha borrado el cliente número ' + req.params.idcliente);
});
